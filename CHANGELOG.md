# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/pecureo/anvil-stream/compare/v0.4.1...v0.5.0) (2018-09-14)


### Bug Fixes

* prevent `_byteLength` from being minified away ([12c7f06](https://gitlab.com/pecureo/anvil-stream/commit/12c7f06))


### Features

* **Issuer:** optionally close popups used in authorization flow ([6bab835](https://gitlab.com/pecureo/anvil-stream/commit/6bab835))



<a name="0.4.1"></a>
## [0.4.1](https://gitlab.com/pecureo/anvil-stream/compare/v0.4.0...v0.4.1) (2018-08-29)


### Bug Fixes

* emit source maps in production ([9bcadb3](https://gitlab.com/pecureo/anvil-stream/commit/9bcadb3))



<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/pecureo/anvil-stream/compare/v0.3.0...v0.4.0) (2018-08-29)


### Bug Fixes

* **Issuer:** migrate left over `session$.error` to `error$.next` ([50c019e](https://gitlab.com/pecureo/anvil-stream/commit/50c019e))


### Features

* **Session:** add `expired$` ([2c80a95](https://gitlab.com/pecureo/anvil-stream/commit/2c80a95))
* **Session:** expose `access_token` ([b20e5d1](https://gitlab.com/pecureo/anvil-stream/commit/b20e5d1))
* **Session:** expose the 'subject UUID' ([ecd61ad](https://gitlab.com/pecureo/anvil-stream/commit/ecd61ad))



<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/pecureo/anvil-stream/compare/v0.2.0...v0.3.0) (2018-06-27)


### Bug Fixes

* **Issuer:** ignore and warn when saving expired sessions ([23f5c47](https://gitlab.com/pecureo/anvil-stream/commit/23f5c47))


### Features

* **Issuer:** add `error$` to emit errors while maintaining `session$` ([66aba8d](https://gitlab.com/pecureo/anvil-stream/commit/66aba8d))



<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/pecureo/anvil-stream/compare/v0.1.1...v0.2.0) (2018-01-10)


### Bug Fixes

* **Issuer:** get secret from original cookie before clearing ([921a63c](https://gitlab.com/pecureo/anvil-stream/commit/921a63c))


### Features

* **Issuer:** make forcing a provider configurable ([cfdc390](https://gitlab.com/pecureo/anvil-stream/commit/cfdc390))



<a name="0.1.1"></a>
## [0.1.1](https://gitlab.com/pecureo/anvil-stream/compare/v0.1.0...v0.1.1) (2018-01-10)


### Bug Fixes

* **Issuer:** prevent secret cookie from being cleared on `force_path` ([58dbdef](https://gitlab.com/pecureo/anvil-stream/commit/58dbdef))



<a name="0.1.0"></a>
# 0.1.0 (2017-05-14)

* chore(anvil-stream): release v0.1.0 ([1b140a3](https://gitlab.com/pecureo/anvil-stream/commit/1b140a3))
* chore(anvil-stream): update build configuration to best practices ([58b3440](https://gitlab.com/pecureo/anvil-stream/commit/58b3440))
* chore(anvil-stream): update dependencies ([53a2594](https://gitlab.com/pecureo/anvil-stream/commit/53a2594))
* chore(authorization): add webpack + TypeScript build chain ([062dda6](https://gitlab.com/pecureo/anvil-stream/commit/062dda6))
* chore(authorization): install Bower dependencies on `npm install` ([39701c6](https://gitlab.com/pecureo/anvil-stream/commit/39701c6))
* docs(LICENSE): add MIT license and note licenses of libraries in `/dist` ([a70d5e8](https://gitlab.com/pecureo/anvil-stream/commit/a70d5e8))
* docs(README): add basic operation instructions ([4ad3d7c](https://gitlab.com/pecureo/anvil-stream/commit/4ad3d7c))
* fix(anvil-stream): be stricter in popup authorization response parsing ([4f3169e](https://gitlab.com/pecureo/anvil-stream/commit/4f3169e))
* fix(authorization): install 'crypto-js' from correct location ([7c7beaa](https://gitlab.com/pecureo/anvil-stream/commit/7c7beaa))
* fix(authorization): pass ES6 code through Babel for UglifyJS compatibility ([59adba9](https://gitlab.com/pecureo/anvil-stream/commit/59adba9))
* fix(Issuer): remove aborted sessions from storage ([c529f37](https://gitlab.com/pecureo/anvil-stream/commit/c529f37))
* fix(Issuer): verify presence of authorization data before parsing ([0177336](https://gitlab.com/pecureo/anvil-stream/commit/0177336))
* fix(Session): make abort check for expiration watcher ([0f2c378](https://gitlab.com/pecureo/anvil-stream/commit/0f2c378))
* fix(Session): mark `abort`ed `Session`s `expired` ([397ee98](https://gitlab.com/pecureo/anvil-stream/commit/397ee98))
* feat(authorization): add popup flow ([cad5736](https://gitlab.com/pecureo/anvil-stream/commit/cad5736))
* feat(authorization): add streaming authorization framework ([56a69a7](https://gitlab.com/pecureo/anvil-stream/commit/56a69a7))
* feat(authorization): allow 'empty' `Session`s to be constructed ([6a07f45](https://gitlab.com/pecureo/anvil-stream/commit/6a07f45))
* feat(Issuer): allow overriding the path for the 'secret' cookie ([c05211e](https://gitlab.com/pecureo/anvil-stream/commit/c05211e))
* feat(Issuer): keep reference to persisted session ([5c380cb](https://gitlab.com/pecureo/anvil-stream/commit/5c380cb))
* feat(Issuer): make authorization data extraction more robust ([dd38f54](https://gitlab.com/pecureo/anvil-stream/commit/dd38f54))
* feat(Session): add `persisted` for checking persistence ([9ac6a90](https://gitlab.com/pecureo/anvil-stream/commit/9ac6a90))
* feat(Session): determine expiration dynamically using the expiration date ([30d4156](https://gitlab.com/pecureo/anvil-stream/commit/30d4156))
* feat(Session): make current scopes available synchronously ([291fde0](https://gitlab.com/pecureo/anvil-stream/commit/291fde0))
* feat(Session): store back reference to `Issuer` ([cf944e4](https://gitlab.com/pecureo/anvil-stream/commit/cf944e4))
* style(Issuer): add missing semicolon ([4a69b7a](https://gitlab.com/pecureo/anvil-stream/commit/4a69b7a))
* style(Issuer): remove unnecessary period ([52a7edf](https://gitlab.com/pecureo/anvil-stream/commit/52a7edf))
* refactor(anvil-stream): install Bower dependencies on `prepublish` instead of `postinstall` ([d928677](https://gitlab.com/pecureo/anvil-stream/commit/d928677))
* refactor(authorization): do not depend explicitly on libraries available on `window` ([780ad90](https://gitlab.com/pecureo/anvil-stream/commit/780ad90))
* refactor(authorization): make `max_age` an 'Authorization Option' ([ca6f406](https://gitlab.com/pecureo/anvil-stream/commit/ca6f406))
* refactor(authorization): replace active session by default ([35e6e72](https://gitlab.com/pecureo/anvil-stream/commit/35e6e72))
* refactor(Issuer): clean up 'session options' adding explicit defaults ([44222da](https://gitlab.com/pecureo/anvil-stream/commit/44222da))
