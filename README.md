# Stream-based Authorization Framework for Anvil Connect

This is a library implementing authorization/anthentication flows for [Anvil
Connect](http://anvil.io/) based on [Rx.js](https://reactivex.io/rxjs).

## Instantiating an `Issuer`

`Issuer` instances form the basis of the authorization flows. They need to be
passed an object with the following interface to be able to operate:
```
OidcConfiguration {
  client_id: string;
  issuer: string;
  redirect_uri: string;
}
```
Optionally a `client_secret` key may be included as well, but this will
typically not be necessary for applications running 'client-side'.

## Authorization

Authorization flows can be instantiated in two different forms, either using
full page navigation or using a pop-up. These methods take at least an array of
'scopes', and optionally an object adhering to the following interface:
```
export interface AuthorizationOptions {
  // Whether to close any popups used in the authorization process (defaults to
  // true). When disabled, requires additional logic in the authorization flow
  // callback handling to close the popup
  close_popup?: boolean;

  // Can be used to force a path on the cookie used to store the secret
  // encrypting the tokens stored in localStorage. This can be useful when
  // running the authorization flow from a sub-path of a domain while
  // authorizing for the entire domain
  force_path?: string;

  // The time in seconds for which the session may stay active
  max_age?: number;

  // By specifying a provider, the authorization process will only use that
  // specific provider instead of showing a selector providing access to all
  // identity providers available to the issuer
  provider?: string;

  // Whether to replace the active 'persisted' (i.e. across page reloads)
  // session
  replace_active_session?: boolean;

  // Setting to true implies replacing the active session
  retrieve_user_info?: boolean;
}
```

### Authorization flow using full-page navigation

Call `authorizeFullPage` on an `Issuer` instance

### Authorization flow in popup

Call `authorizeWithPopup` on the `Issuer`. The page located at `redirect_uri`
must capture its full URI and 'post' it back to the window that initiated the
authorization flow using code such as
```javascript
window.addEventListener('load', function() {
  var opener = window.opener;
  if(opener) {
    opener.postMessage(window.location.href, opener.location.origin);
    window.close();
  }
});
```
