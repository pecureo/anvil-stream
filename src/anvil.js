// The following are dependencies of Anvil, these are installed through bower
// and need some special care to be loaded properly. A lot of their
// functionality is expected to be on the window object. The following code
// tries to minimize those occurrences

window.sjcl = require('sjcl');

window.CryptoJS = require('exports-loader?CryptoJS!crypto-js/core');
require('crypto-js/sha256');

window.b64tohex = require('exports-loader?b64tohex!jsrsasign/ext/base64');

let jsbn = require('exports-loader?BigInteger&int2char!jsrsasign/ext/jsbn');
window.BigInteger = jsbn.BigInteger;
window.int2char = jsbn.int2char;

let rsa = require('exports-loader?RSAKey&parseBigInt!jsrsasign/ext/rsa');
window.RSAKey = rsa.RSAKey;
window.parseBigInt = rsa.parseBigInt;
window._rsasign_verifySignatureWithArgs = require('exports-loader?_rsasign_verifySignatureWithArgs!jsrsasign/rsasign-1.2');

window.ASN1HEX = require('exports-loader?ASN1HEX!jsrsasign/asn1hex-1.1');
window.KJUR = require('imports-loader?KJUR=>{}!exports-loader?KJUR!jsrsasign/crypto-1.1');

let base64x = require('exports-loader?b64utohex&b64utoutf8!jsrsasign/base64x-1.1');
window.b64utohex = base64x.b64utohex;
window.b64utoutf8 = base64x.b64utoutf8;

window.jsonParse = require('exports-loader?jsonParse!jsjws/ext/json-sans-eval');
require('jsjws/jws-2.0');

module.exports = {
  Anvil: require(`exports-loader?Anvil!imports-loader?jQuery=jquery,$=jquery!anvil-connect`),
  CryptoJS: window.CryptoJS,
  JWS: window.KJUR.jws.JWS,
  b64utohex: base64x.b64utohex
};
