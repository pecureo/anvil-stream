export class Scopes {
  public length: number;
  public scopes: string[];

  constructor(scopes_string: string) {
    if(scopes_string && scopes_string.length > 0) {
      this.scopes = scopes_string.split(' ');
    } else {
      this.scopes = [];
    }

    this.length = this.scopes.length;
  }

  includes(...requested_scopes: string[]) {
    return requested_scopes.every(requested_scope => {
      return (this.scopes.indexOf(requested_scope) >= 0);
    });
  }
}
