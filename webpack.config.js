'use strict';

const path = require('path');

module.exports = {
  entry: {
    index: path.resolve(__dirname, 'src', 'index.ts'),
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        include: [ path.resolve(__dirname, 'src') ],
        use: [ 'ts-loader' ]
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    library: 'AnvilStream',
    libraryTarget: 'umd'
  },
  resolve: {
    alias: {
      'anvil-connect': path.resolve(__dirname, 'bower_components', 'anvil-connect'),
      'crypto-js': path.resolve(__dirname, 'bower_components', 'crypto-js-lib/components'),
      jsrsasign: path.resolve(__dirname, 'bower_components', 'jsrsasign'),
      jsjws: path.resolve(__dirname, 'bower_components', 'jsjws'),
      sjcl: path.resolve(__dirname, 'bower_components', 'sjcl')
    },
    extensions: [ '.js', '.ts' ]
  }
};
